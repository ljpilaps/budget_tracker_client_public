import { Fragment, useState, useEffect, useContext } from 'react'; 
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../contexts/UserContext'; 

export default () => {
	const { user } = useContext(UserContext)
	const { isExpanded, setIsExpanded } = useState(false)
	//lets declare 2 variables that will describe 2 sections of our navbar
	let RightNavOptions
	let LeftNavOptions

	//lets create a control structure which will determine the options displayed in the navbar.

	if(user.email !== null) {
		RightNavOptions = (
			<Fragment>
				<Link href='/'>
					<a className='nav-link'>
						Logout
					</a>
				</Link>
			</Fragment>
		)
		LeftNavOptions = (
			<Fragment>
				<Link href='/user/categories'>
					<a className='nav-link'>
						Categories
					</a>
				</Link>
				<Link href='/user/records'>
					<a className='nav-link'>
						Records
					</a>
				</Link>
				<Link href='/user/charts/category-breakdown'>
					<a className='nav-link'>
						Breakdown
					</a>
				</Link>
			</Fragment>
		)
	} else {
		RightNavOptions = null
		LeftNavOptions = null
	}


   return(
   	<Fragment>
	   	<Navbar expanded={ isExpanded } bg="dark" expand="lg" fixed="top" variant="dark">
	   	 <Link href="/">
	   	     <a className="navbar-brand">INSERT YOUR APP NAME</a>
	   	 </Link>
	   	 <Navbar.Toggle onClick={ () => setisExpanded(!isExpanded) } aria-controls="basic-navbar-nav" />
	   	 <Navbar.Collapse className="basic-navbar-nav">
	        <Nav className="mr-auto" onClick={ () => setisExpanded(!isExpanded) }>
	        	{ RightNavOptions }
	        </Nav>
	        <Nav className='ml-auto' onClick={ () => setisExpanded(!isExpanded) }>
	        	{ LeftNavOptions }
	        </Nav>
	   	 </Navbar.Collapse>
	   	</Navbar>
   	</Fragment>
   	)
}