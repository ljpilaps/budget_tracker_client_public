import React from 'react';

const UserContext = React.createContext()

// context provider component allows context to change subscriptions of child components.
export const UserProvider = UserContext.Provider

//exported the context component as default obtject to be exported.
export default UserContext